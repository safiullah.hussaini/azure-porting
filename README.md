---
platform: arduino ide
device: arduino nano 33 iot
language: c
---

Run a simple C sample on Arduino Nano 33 IoT
===
---

# Table of Contents

- [Introduction](#Introduction)
- [Step 1: Prerequisites](#Step-1-Prerequisites)
- [Step 2: Prepare your Device](#Step-2-PrepareDevice)
- [Step 3: Build and Run the Sample](#Step-3-Build)
-   [Next Steps](#NextSteps)

<a name="Introduction"></a>
# Introduction

**About this document**

This document describes the process of connecting an Arduino NANO 33 IoT based system to the Azure IoT Hub. This multi-step process includes:
- Configuring Azure IoT Hub
- Registering your IoT device
- Build and deploy Azure IoT SDK on device

<a name="Step-1-Prerequisites"></a>
# Step 1: Prerequisites

You should have the following items ready before beginning the process:
- Computer with Git client installed and access to the
  [azure-iot-sdk-c](https://github.com/Azure/azure-iot-sdk-c) GitHub public repository.
- A computer with the [Arduino IDE](https://www.arduino.cc/en/Main/Software) version 1.6.6 or later installed.
- One of the following:
  - [Arduino NANO 33 IOT](https://store.arduino.cc/usa/nano-33-iot)
-   [Setup your IoT hub][lnk-setup-iot-hub]
-   [Provision your device and get its credentials][lnk-manage-iot-hub]

#### IMPORTANT NOTE
The sample won't run on older 8-bit AVR-based Arduinos (like the UNO or Mega 2560). A modern 32-bit ARM Cortex M0+ MCU is required (e.g. Atmel’s SAMD21 MCU).

<a name="Step-2-PrepareDevice"></a>
# Step 2: Prepare your Device

This section shows you how to set up a development environment for the Azure IoT device SDK with the Arduino NANO 33 IoT board.

1. Open the Arduino IDE and install the following libraries using the ```Library Manager```, which can be accessed via ```Sketch -> Include Library -> Managed Libraries ...```
  * ```WiFiNINA```
  * ```RTCZero```
  * ```AzureIoTHub```
  * ```AzureIoTProtocol_HTTP```
  * ```AzureIoTProtocol_MQTT```
  * ```AzureIoTUtility```
2. Flash the root certificate of the Azure IoT hub host, ```<name>.azure-devices.net``` found in Connection String from [earlier](#beforebegin), using the instructions available on the ["Firmware/Certificates updater for the Arduino WifiNINA Shield"](https://github.com/arduino/FirmwareUpdater#usage) page.
3. Carefully replace "WiFi101.h" to "WiFiNINA.h" in the libraries as nano 33 iot board supports WiFiNINA library
<a name="Step-3-Build"></a>
# Step 3: Build and Run the sample

1. In the Arduino IDE, select ```Arduino NANO 33 IOT``` as the board in the ```Tools -> Board``` menu.
2. Open the ```iothub_ll_telemetry_sample``` example in the Arduino IDE, ```File -> Examples -> AzureIoTHub -> iothub_ll_telemetry_sample```
3. In ```iot_configs.h```, update the following line with your WiFi accesspoint's SSID:
   ```
   char ssid[] = "yourNetwork"; //  your network SSID (name)
   ```
4. Update the following line with your WiFi accesspoint's password:
   ```
   char pass[] = "secretPassword";    // your network password (use for WPA, or use as key for WEP)
   ```
5. Locate the following code in the ```iot_configs.h```:
   ```
   static const char* connectionString = "[device connection string]";
   ```
6. Replace `[device connection string]` with the device connection string you noted [earlier](#Step-1-Prerequisites). 
7. Add this block of code at the top of ```iothub_ll_telemetry_sample.ino``` :
   ```#ifdef is_arduino_board```
      ```#include "Arduino.h"```
   ```#endif```
8. Add this block of code at the end before ```#endif // SAMPLE_INIT_H``` in ```sample_init.h```:

   ```#if defined(ARDUINO_ARCH_SAMD)```
   ```#define sample_init m0_sample_init```
   ```#define is_arduino_board```
   ```void m0_sample_init(const char* ssid, const char* password);```
   ```#endif```      
9. Save the changes. Press the ```Verify``` button in the Arduino IDE to build the sample sketch.  

<a name="deploy"/>
## Deploy the sample

1. Follow the steps from the [build](#Step-3-Build) section.
2. If you are using the Zero, connect the board to the WiFi Shield 101 and then connect the programming port of the board to your computer with a USB cable. For the MKR1000, connect the board to your computer with a USB cable.
3. Select serial port associated with the board in the ```Tools -> Port``` menu.
4. Press the ```Upload``` button in the Arduino IDE to build and upload the sample sketch.

<a name="run"/>
## Run the sample

1. Follow the steps from the [deploy](#deploy) section.

7.   See [Manage IoT Hub][lnk-manage-iot-hub] to learn how to observe the messages IoT Hub receives from the **simplesample_http** application and how to send cloud-to-device messages to the **simplesample_http** application.

<a name="NextSteps"></a>
# Next Steps

You have now learned how to run a sample application that collects sensor data and sends it to your IoT hub. To explore how to store, analyze and visualize the data from this application in Azure using a variety of different services, please click on the following lessons:

-   [Manage cloud device messaging with iothub-explorer]
-   [Save IoT Hub messages to Azure data storage]
-   [Use Power BI to visualize real-time sensor data from Azure IoT Hub]
-   [Use Azure Web Apps to visualize real-time sensor data from Azure IoT Hub]
-   [Weather forecast using the sensor data from your IoT hub in Azure Machine Learning]
-   [Remote monitoring and notifications with Logic Apps]   

[Manage cloud device messaging with iothub-explorer]: https://docs.microsoft.com/en-us/azure/iot-hub/iot-hub-explorer-cloud-device-messaging
[Save IoT Hub messages to Azure data storage]: https://docs.microsoft.com/en-us/azure/iot-hub/iot-hub-store-data-in-azure-table-storage
[Use Power BI to visualize real-time sensor data from Azure IoT Hub]: https://docs.microsoft.com/en-us/azure/iot-hub/iot-hub-live-data-visualization-in-power-bi
[Use Azure Web Apps to visualize real-time sensor data from Azure IoT Hub]: https://docs.microsoft.com/en-us/azure/iot-hub/iot-hub-live-data-visualization-in-web-apps
[Weather forecast using the sensor data from your IoT hub in Azure Machine Learning]: https://docs.microsoft.com/en-us/azure/iot-hub/iot-hub-weather-forecast-machine-learning
[Remote monitoring and notifications with Logic Apps]: https://docs.microsoft.com/en-us/azure/iot-hub/iot-hub-monitoring-notifications-with-azure-logic-apps
[lnk-setup-iot-hub]: ../setup_iothub.md
[lnk-manage-iot-hub]: ../manage_iot_hub.md
